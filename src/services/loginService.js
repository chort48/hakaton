import api from '../fetch/fetch.js';

class loginService{
    async logIn(data){
        try {
            const res = await api('login', 'POST', {body:data});
            if ('error' in res) {

            }
            return await res.data;
        }
        catch(e) {
            return e.message;
        }
    }
    async logout(){
        try {
            const res = await api('logout', 'POST', {body:localStorage.token});
            if ('error' in res) {

            }
            return await res.data;
        }
        catch(e) {
            return e.message;
        }
    }
    async registration(data){
        try {
            const res = await api('registration', 'POST', {body:data});
            if ('error' in res) {

            }
            return await res.data;
        }
        catch(e) {
            return e.message;
        }
    }
}
export default new loginService ()
