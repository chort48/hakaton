import api from '../fetch/fetch.js';

class posterService {
    async getMainAll(){
        try {
            const res = await api('poster_main', 'GET');
            if ('error' in res) {
                alert(res.error)
            }
            return await res.data;
        }
        catch(e) {
            return e.message;
        }
    }

    async getAll(skip, take, id){
        try {
            let url = `poster`
            if(skip || take || id ){
                url = `poster?`
            }
            if(skip){
                if(url !== 'poster?'){
                    url += `&skip=${skip}`
                }
                else{
                    url += `skip=${skip}`
                }
            }
            if(take){
                if(url !== 'poster?'){
                    url += `&take=${take}`
                }
                else{
                    url += `take=${take}`
                }
            }
            if(id){
                if(url !== 'poster?'){
                    url += `&filter=${id}`
                }
                else{
                    url += `filter=${id}`
                }
            }
            const res = await api(url, 'GET');
            if ('error' in res) {
                alert(res.error)
            }
            return await res.data.data;
        }
        catch(e) {
            return e.message;
        }
    }
    async getDetail(id){
        try {
            let url = `poster/${id}`
            const res = await api(url, 'GET');
            if ('error' in res) {
                alert(res.error)
            }
            return await res.data.data;
        }
        catch(e) {
            return e.message;
        }
    }
    async getSearch(key){
        try {
            const res = await api(`search?key=${key}`, 'GET');
            if ('error' in res) {
                alert(res.error)
            }
            return await res.data.data;
        }
        catch(e) {
            return e.message;
        }
    }
    async getCategory(id){
        try {
            const res = await api(`category/${id}`, 'GET');
            if ('error' in res) {
                alert(res.error)
            }
            return await res.data;
        }
        catch(e) {
            return e.message;
        }
    }
}

export default new posterService()
