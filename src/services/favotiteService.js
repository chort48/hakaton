import api from '../fetch/fetch.js';

class favoriteService {
    async addFavorite(data){
        try {
            const res = await api('add_favorite', 'POST', {body:data});
            if ('error' in res) {

            }
            return await res.data.data;
        }
        catch(e) {
            return e.message;
        }
    }
    async deletFavorite(data){
        data.set('_method', 'DELETE')
        try {
            const res = await api('del_favorite', 'POST', {body:data});
            if ('error' in res) {

            }
            return await res.data.data;
        }
        catch(e) {
            return e.message;
        }
    }
    async getFavorite(){
        try {
            const res = await api(`favorite?user_id=${localStorage.id}`, 'GET');
            if ('error' in res) {

            }
            return await res.data.data;
        }
        catch(e) {
            return e.message;
        }
    }
}

export default new favoriteService()
