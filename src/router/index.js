import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: () => import('@/views/Home')
  },
  {
    path: '/categories/:id',
    name: 'Categories',
    component: () => import('@/views/Categories')
  },
  {
    path: '/detailposters:id',
    name: 'DetailPosters',
    component: () => import('@/views/DetailPosters.vue')
  },
  {
    path: '/allposters',
    name: 'AllPosters',
    component: () => import('@/views/AllPosters.vue')
  },
  {
    path: '/login',
    name: 'Login',
    meta:{auth:true},
    component: () => import('@/views/Login.vue')
  },
  {
    path: '/registration',
    name:'Registration',
    meta: {auth:true},
    component:()=>import('@/views/Registration.vue')
  },
  {
    path: '/favorite',
    name:'Favorite',
    component:()=>import('@/views/Favorite.vue')
  }
  /*{
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/!* webpackChunkName: "about" *!/ '../views/About.vue')
  }*/
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})
router.beforeEach((to, from, next)=>{
  const requireAuth_1 = to.matched.some(login => login.meta.auth)
  const requireAuth_2 = to.matched.some(registration => registration.meta.auth)
  if(localStorage.token !== '' && (requireAuth_1 || requireAuth_2)){
    next('/')
  }
  else{
    next()
  }
})
export default router
