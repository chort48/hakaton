export function dateFormat (date) {
    let date_output
    let year = ""
    let month = ""
    let month_new = ""
    let day = ""
    let hour = ""
    let minut = ""
    let sec = ""
    let k = 0
    for(let i = 0; i < date.length; ++i){
        if(k < 3){
            if(date[i] !== '-' && k === 0){
                year += date[i]
            }
            else if(k === 0){
                k++
            }
            else if(date[i] !== '-' && k === 1){
                month += date[i]
                if(month.length === 2){
                    month_new = monthFormat(month)
                }
            }
            else if(k === 1){
                k++
            }
            else if(date[i] !== 'T' && k === 2){
                day += date[i]
            }
            else if(k === 2){
                k++;
            }
        }
        else{
            if(date[i] !== ':' && k === 3){
                hour += date[i]
            }
            else if(k === 3){
                k++
            }
            else if(date[i] !== ':' && k === 4){
                minut += date[i]
            }
            else if(k === 4){
                k++
            }
            else if(date[i] !== 'Z' && k === 5){
                sec += date[i]
            }
            else if(k === 5){
                k++
            }
        }
    }
    date_output = day +" "+ month_new +" "+ year + " " + hour+ ":" + minut
    let date_filter = new Date(date)
    return new Object({date_output, date_filter})
}
export function monthFormat (month) {
    let month_new = ""
    if(month === '01'){
        month_new = "Января"
    }
    if(month === '02'){
        month_new = 'Февраля'
    }
    if(month === '03'){
        month_new = 'Марта'
    }
    if(month === '04'){
        month_new = 'Апреля'
    }
    if(month === '05'){
        month_new = 'Мая'
    }
    if(month === '06'){
        month_new = 'Июня'
    }
    if(month === '07'){
        month_new = 'Июля'
    }
    if(month === '08'){
        month_new = 'Августа'
    }
    if(month === '09'){
        month_new = 'Сентября'
    }
    if(month === '10'){
        month_new = 'Октября'
    }
    if(month === '11'){
        month_new = 'Ноября'
    }
    if(month === '12'){
        month_new = 'Декабря'
    }
    return month_new
}
