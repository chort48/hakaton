export default async function (url,method = 'GET', con={}) {
    try {
        let res = await fetch(`http://172.20.10.3:8000/api/${url}`, {
            method,
            headers:{
                'Accept':'application/json'
            },
            body: con.body
        })
        let data = await res.json()
        return {data, res}
    } catch (e) {
        return {error: e}
    }
}
